<?php

namespace App\Repositories;

use App\Models\Tag;

class TagsRepository extends BaseRepository{

    public function __construct(Tag $model){
        $this->model = $model;
    }

    public function getAllTagsIds(){

        $allTags = $this->getAll();

        $tagsIds = array();
        
        foreach ($allTags as $tag) {

            array_push($tagsIds, $tag->id);
        }

        return $tagsIds;
    }
}