<?php

namespace App\Repositories;

use App\Models\Equipment;
use App\Models\Tag;

class EquipmentRepository extends BaseRepository{

    public function __construct(Equipment $model){
        $this->model = $model;
    }

    public function getEquipmentsWithTags($tags){

        $equipments = $this->getAll();
        
        $filtered = array();
        $equipmentAdded = false;

        foreach ($equipments as $equipment){

            foreach ($tags as $tag){

                foreach ($equipment->tags as $eqtag){
                    if ($eqtag->id == $tag){
                        array_push($filtered, $equipment);
                        $equipmentAdded = true;
                        break;
                    }
                }

                if ($equipmentAdded == true){
                    $equipmentAdded = false;
                    break;
                }
            }
        }
        
        return $filtered;
    }

    public function getAvailableEquipments(){
        return $this->getAll()->where('available_quantity', '>', 0);
    }
}