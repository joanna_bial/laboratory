<?php

namespace App\Repositories;

use App\Models\EquipmentPiece;

class EquipmentPieceRepository extends BaseRepository{

    public function __construct(EquipmentPiece $model){
        $this->model = $model;
    }

    public function getPiecesForEquipment($equipmentId) {
        return $this->getAll()->where('equipment_id', '=', $equipmentId);
    }
}