<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository{

    public function __construct(User $model){
        $this->model = $model;
    }

    public function getAllStudents($columns = array('*')){
        return $this->model->where('status', 'student')->orderBy('last_name', 'asc')->get();
    }

    public function getAllAssistants($columns = array('*')){
        return $this->model->where('status', 'assistant')->orderBy('last_name', 'asc')->get();
    }
}