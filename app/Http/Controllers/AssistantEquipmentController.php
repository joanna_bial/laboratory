<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Repositories\EquipmentRepository;
use App\Repositories\TagsRepository;
use App\Models\Equipment;
use App\Models\EquipmentPiece;
use DB;
use Session;
use Intervention\Image\Facades\Image;

class AssistantEquipmentController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(EquipmentRepository $equipmentRepo, TagsRepository $tagsRepo){
        
        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $allTags = $tagsRepo->getAll();
        
        if(Session::has('selectedTags')){
            $selectedTags = Session::get('selectedTags');
            $equipments = $equipmentRepo->getEquipmentsWithTags($selectedTags);
        }
        else{
            $selectedTags = array();
            $equipments = $equipmentRepo->getAll();
        }

        $selectedTagsIds = $selectedTags;
        
        return view('assistant.equipments.list', ["equipmentsList" => $equipments, "allTags" => $allTags, "selectedTagsIds" => $selectedTagsIds]);
    }

    public function filter(EquipmentRepository $equipmentRepo, TagsRepository $tagsRepo, Request $request){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $selectedTags = $request->get('selectedTags');

        Session::flash('selectedTags', $selectedTags);

        return redirect()->action('App\Http\Controllers\AssistantEquipmentController@index');
    }

    public function edit(EquipmentRepository $equipmentRepo, TagsRepository $tagsRepo, $id){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $equipment = $equipmentRepo->find($id);
        $allTags = $tagsRepo->getAll();

        return view('assistant.equipments.edit', ["equipment"=>$equipment, "allTags" => $allTags]);
    }

    public function editStore(EquipmentRepository $equipmentRepo, Request $request){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $equipment = $equipmentRepo->find($request->input('id'));

        if (!is_null($equipment)){
            $equipment->name = $request->input('equipmentName');
            $equipment->description = $request->input('description');
            $equipment->model = $request->input('model');
            
            if ($request->image != null){
                $request->validate([
                    'image' => 'mimes:jpeg,bmp,png' // Only allow .jpg, .bmp and .png file types.
                ]);    

                $imagePath = "storage/" . request('image')->store('uploads', 'public');

                $image = Image::make(public_path($imagePath))->resize(300,300);
                $image->save();

                $equipment->picture_path = $imagePath;
            }
            
            $equipment->producent_url = $request->input('producent_url');
    
            $equipment->save();
            $equipment->tags()->sync($request->input('tags'));
        }

        return redirect()->action('App\Http\Controllers\AssistantEquipmentController@index');
    }

    public function create(TagsRepository $tagsRepo){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $allTags = $tagsRepo->getAll();

        return view('assistant.equipments.create', ["allTags" => $allTags]);
    }

    public function store(Request $request){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $equipment = new Equipment;

        $equipment->name = $request->input('equipmentName');
        $equipment->description = $request->input('description');
        $equipment->model = $request->input('model');
        
        if ($request->image != null){
            $request->validate([
                'image' => 'mimes:jpeg,bmp,png' // Only allow .jpg, .bmp and .png file types.
            ]);    
                
                $imagePath = "storage/" . request('image')->store('uploads', 'public');

                $image = Image::make(public_path($imagePath))->resize(300,300);
                $image->save();

                $equipment->picture_path = $imagePath;
        }
        
        $equipment->producent_url = $request->input('producent_url');
        $equipment->save();
        $equipment->tags()->sync($request->input('tags'));

        $equipmentPieces = array();

        for ($i = 1; $i <= $request->input('quantity'); $i++) {

            $equipmentPiece = new EquipmentPiece;
            $equipmentPiece->number = $equipment->name.'_'.$equipment->model .'_'.$i;

            array_push($equipmentPieces, $equipmentPiece);
        }

        $equipment->pieces()->saveMany($equipmentPieces);

        return redirect()->action('App\Http\Controllers\AssistantEquipmentController@index');
    }

    public function delete(EquipmentRepository $equipmentRepo, $id){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $equipment = $equipmentRepo->delete($id);

        return redirect()->action('App\Http\Controllers\AssistantEquipmentController@index');
    }

    public function raport(EquipmentRepository $equipmentRepo, TagsRepository $tagsRepo, Request $request){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $equipments = $equipmentRepo->getAll();

        return view('assistant.equipments.raport', ["equipmentsList" => $equipments]);
    }
}
