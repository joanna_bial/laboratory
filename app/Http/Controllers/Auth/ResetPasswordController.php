<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Repositories\UserRepository;

class ResetPasswordController extends Controller
{
    public function index(){
        
        return view('auth.passwords.reset');
    }

    public function update(Request $request, UserRepository $userRepo){
        
        $user = $userRepo->find($request->input('id'));

        $user->password = Hash::make($request->input('password'));

        $user->save();

        if ($user->status == 'assistant'){
            
            return redirect()->action('App\Http\Controllers\AssistantEquipmentController@index');
        }

        return redirect()->action('App\Http\Controllers\StudentEquipmentController@index');
    }
}
