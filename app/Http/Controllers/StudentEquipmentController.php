<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Repositories\EquipmentRepository;
use App\Repositories\TagsRepository;
use App\Models\Equipment;
use DB;
use Session;

class StudentEquipmentController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(EquipmentRepository $equipmentRepo, TagsRepository $tagsRepo){
        
        if (Auth::user()->status != 'student'){
            return redirect()->route('login');
        }

        $allTags = $tagsRepo->getAll();
        
        if(Session::has('selectedTags')){
            $selectedTags = Session::get('selectedTags');
            $equipments = $equipmentRepo->getEquipmentsWithTags($selectedTags);
        }
        else{
            $selectedTags = $tagsRepo->getAllTagsIds();
            $equipments = $equipmentRepo->getAll();
        }

        $selectedTagsIds = $selectedTags;
        
        return view('student.equipments.list', ["equipmentsList" => $equipments, "allTags" => $allTags, "selectedTagsIds" => $selectedTagsIds]);
    }

    public function filter(EquipmentRepository $equipmentRepo, TagsRepository $tagsRepo, Request $request){

        if (Auth::user()->status != 'student'){
            return redirect()->route('login');
        }
        
        $selectedTags = $request->get('selectedTags');

        Session::flash('selectedTags', $selectedTags);

        return redirect()->action('App\Http\Controllers\StudentEquipmentController@index');
    }
}
