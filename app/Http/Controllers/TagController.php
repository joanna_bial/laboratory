<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Repositories\TagsRepository;
use App\Models\Tag;

class TagController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(TagsRepository $tagsRepo){
        
        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $allTags = $tagsRepo->getAll();
        
        return view('assistant.tags.list', ["tagsList" => $allTags]);
    }

    public function create(){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        return view('assistant.tags.create');
    }

    public function store(Request $request){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $tag = new Tag;

        $tag->name = $request->input('name');
        $tag->save();
    
        return redirect()->action('App\Http\Controllers\TagController@index');
    }

    public function edit(TagsRepository $tagsRepo, $id){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $tag = $tagsRepo->find($id);

        return view('assistant.tags.edit', ["tag"=>$tag]);
    }

    public function editStore(TagsRepository $tagsRepo, Request $request){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $tag = $tagsRepo->find($request->input('id'));

        $tag->name = $request->input('name');
        $tag->save();
    
        return redirect()->action('App\Http\Controllers\TagController@index');
    }

    public function delete(TagsRepository $tagsRepo, $id){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }
        
        $tag = $tagsRepo->delete($id);

        return redirect()->action('App\Http\Controllers\TagController@index');
    }
}
