<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Repositories\UserRepository;
use App\Models\User;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(UserRepository $userRepo){
        
        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $allStudents = $userRepo->getAllStudents();
        $allAssistants = $userRepo->getAllAssistants();
        
        return view('assistant.users.list', ["studentsList" => $allStudents, "assistantsList" => $allAssistants]);
    }

    public function createStudent(){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        return view('assistant.users.createstudent');
    }

    public function storeStudent(Request $request){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $student = new User;

        $student->first_name = $request->input('firstname');
        $student->last_name = $request->input('lastname');
        $student->email = $request->input('email');
        $student->login = $request->input('firstname').''.$request->input('lastname');
        $student->password = Hash::make("123456789");
        $student->status = "student";
        
        $student->save();
    
        return redirect()->action('App\Http\Controllers\UserController@index');
    }

    public function createAssistant(){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        return view('assistant.users.createassistant');
    }

    public function storeAssistant(Request $request){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $assistant = new User;

        $assistant->first_name = $request->input('firstname');
        $assistant->last_name = $request->input('lastname');
        $assistant->email = $request->input('email');
        $assistant->login = $request->input('firstname').''.$request->input('lastname');
        $assistant->password = Hash::make("123456789");
        $assistant->status = "assistant";
        
        $assistant->save();
    
        return redirect()->action('App\Http\Controllers\UserController@index');
    }

    public function editStudent(UserRepository $userRepo, $id){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $student = $userRepo->find($id);

        return view('assistant.users.editstudent', ["student"=>$student]);
    }

    public function editStudentStore(UserRepository $userRepo, Request $request){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $student = $userRepo->find($request->input('id'));

        $student->first_name = $request->input('firstname');
        $student->last_name = $request->input('lastname');
        $student->email = $request->input('email');
        $student->save();
    
        return redirect()->action('App\Http\Controllers\UserController@index');
    }

    public function deleteStudent(UserRepository $userRepo, $id){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }
        
        $student = $userRepo->delete($id);

        return redirect()->action('App\Http\Controllers\UserController@index');
    }
}
