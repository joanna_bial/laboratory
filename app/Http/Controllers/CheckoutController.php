<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Repositories\UserRepository;
use App\Repositories\EquipmentRepository;
use App\Repositories\EquipmentPieceRepository;
use App\Repositories\CheckoutRepository;

use App\Models\Checkout;

class CheckoutController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(UserRepository $userRepo){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $allStudents = $userRepo->getAllStudents();
        
        return view('assistant.checkouts.list', ["studentsList" => $allStudents]);
    }

    public function studentCheckouts($id, UserRepository $userRepo){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $student = $userRepo->find($id);
        $checkouts = $student->checkouts;

        return view('assistant.checkouts.student', ["student" => $student, "checkouts" => $checkouts]);
    }

    public function newCheckout($id, EquipmentRepository $equipmentRepo, UserRepository $userRepo){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $availableEquipments = $equipmentRepo->getAvailableEquipments();
        $student = $userRepo->find($id);

        return view('assistant.checkouts.create', ["student" => $student, "allEquipments" => $availableEquipments]);
    }

    public function store(Request $request, EquipmentPieceRepository $equipmentPieceRepo, CheckoutRepository $checkoutRepo, UserRepository $userRepo){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }

        $allPieces = $equipmentPieceRepo->getPiecesForEquipment($request->input('equipment_id'));

        foreach ($allPieces as $piece){
            if ($piece->checkouts_quantity == 0){
                $pieceToBorrow = $piece;
                break;
            }
        }

        $checkout = new Checkout;
        $checkout->equipment_piece_id = $pieceToBorrow->id;
        $checkout->student_id = $request->input('student_id');

        $checkout->save();

        $student = $userRepo->find($request->input('student_id'));
        $checkouts = $student->checkouts;
    
        return redirect()->action('App\Http\Controllers\CheckoutController@studentCheckouts', [$student->id]);
    }

    public function remove($id, CheckoutRepository $checkoutRepo, UserRepository $userRepo){

        if (Auth::user()->status != 'assistant'){
            return redirect()->route('login');
        }
        
        $checkout = $checkoutRepo->find($id);
        $student = $userRepo->find($checkout->student_id);

        $checkoutRepo->delete($id);

        return redirect()->action('App\Http\Controllers\CheckoutController@studentCheckouts', [$student->id]);
    }
}
