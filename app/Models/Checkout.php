<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'student_id',
        'equipment_piece_id'
    ];

    public function piece(){
        return $this->belongsTo(EquipmentPiece::class, 'equipment_piece_id');
    }

    public function student(){
        return $this->belongsTo(User::class, 'student_id');
    }
}