<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Equipment extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'model',
        'picture_path',
        'description',
        'producent_url'
    ];

    protected $hidden = [
        'id'
    ];

    protected $table = 'equipments';

    public function tags(){
        return $this->belongsToMany(Tag::class, 'equipments_tags');
    }

    public function getTagsIdsAttribute(){
        $tags = $this->tags()->get();

        $tagsIds = array();
        
        foreach ($tags as $tag) {

            array_push($tagsIds, $tag->id);
        }

        return $tagsIds;
    }

    public function pieces(){
        return $this->hasMany(EquipmentPiece::class, 'equipment_id');
    }

    public function getQuantityAttribute(){

        return $this->pieces()->count();
    }

    public function getAvailableQuantityAttribute(){

        $pieces = $this->pieces()->get();

        $available = $this->quantity;

        foreach($pieces as $piece){
            
            $available = $available - $piece->checkouts->count();
        }

        return $available;
    }
}