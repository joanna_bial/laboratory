<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Checkout;
use App\Models\Equipment;

class EquipmentPiece extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'number',
        'equipment_id'
    ];

    public function checkouts(){
        return $this->hasMany(Checkout::class, 'equipment_piece_id');
    }

    public function checkout(){
        return $this->hasOne(Checkout::class, 'equipment_piece_id')->latest();
    }

    public function equipment(){
        return $this->belongsTo(Equipment::class, 'equipment_id');
    }

    public function getCheckoutsQuantityAttribute(){

        return $this->checkouts()->count();
    }
}