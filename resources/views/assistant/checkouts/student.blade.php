@extends('layouts.template')

@section('content')

<div class="main">
    <div class="container">

        <h2>{{$student->first_name}} {{$student->last_name}}</h2>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Nazwa</th>
                    <th scope="col">Model</th>
                    <th scope="col">Numer</th>
                    <th scope="col">Operacje</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($checkouts as $checkout)
                    <tr>
                        <td>{{$checkout->piece->equipment->name}}</td>
                        <td>{{$checkout->piece->equipment->model}}</td>
                        <td>{{$checkout->piece->number}}</td>
                        <td>
                            <div class="btn-group">
                                <a type="button" class="btn btn-success m-1 btn-sm" href="{{ URL::to('assistant/checkouts/remove/' . $checkout->id ) }}">Zwróć</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a class="btn btn-outline-secondary" href="{{ URL::to('assistant/checkouts/new/' . $student->id) }}">Nowe wypożyczenie</a>
    </div>
</div>



@endsection('content')
    