@extends('layouts.template')

@section('content')

<div class="main">
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Imię</th>
                    <th scope="col">Nazwisko</th>
                    <th scope="col">Email</th>
                    <th scope="col">Wypożyczenia</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($studentsList as $student)
                    <tr>
                        <td>{{$student->first_name}}</td>
                        <td>{{$student->last_name}}</td>
                        <td>{{$student->email}}</td>
                        <td>
                            <div class="btn-group">
                                <a type="button" class="btn btn-info m-1 btn-sm" href="{{ URL::to('assistant/checkouts/student/' . $student->id) }}">Wypożyczenia</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>



@endsection('content')
    
