@extends('layouts.template')

@section('content')


<div class="container">
    <h2>{{$student->first_name}} {{$student->last_name}} - wypożycz:</h2>

    <form action="{{ action('App\Http\Controllers\CheckoutController@store') }}" method="POST" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="student_id" value="{{$student->id}}"/>

        <div class="form-group">
            <label for="equipment_id">Element:</label>
            <select name="equipment_id">
            @foreach ($allEquipments as $equipment)
                <option value="{{$equipment->id}}">{{$equipment->name}}</option>
            @endforeach
            </select>
        </div>

        </br>

        <input type="submit" value="Zapisz" class="btn btn-primary"/>

    </form>
</div>


@endsection('content')