@extends('layouts.template')
@section('content')


<div class="container">
    <h2>Dodawanie nowego pracownika laboratorium:</h2>

    <form action="{{ action('App\Http\Controllers\UserController@storeAssistant') }}" method="POST" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <div class="form-group">
            <label for="name">Imię</label>
            <input type="text" class="form-control" name="firstname"/>
        </div>
        <div class="form-group">
            <label for="name">Nazwisko</label>
            <input type="text" class="form-control" name="lastname"/>
        </div>
        <div class="form-group">
            <label for="name">Email</label>
            <input type="text" class="form-control" name="email"/>
        </div>

        <br/>
        <input type="submit" value="Zapisz" class="btn btn-primary"/>

    </form>
</div>


@endsection('content')
    