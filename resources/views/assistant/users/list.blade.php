@extends('layouts.template')

@section('content')

<div class="main">
    <div class="container">
    
        <br/>
        <div class="btn-group" role="group">
            <a class="btn btn-outline-secondary m-1" href="{{ URL::to('assistant/users/createstudent') }}">Dodaj studenta</a>
            <a class="btn btn-outline-secondary m-1" href="{{ URL::to('assistant/users/createassistant') }}">Dodaj pracownika laboratorium</a>
        </div>
    
        <br/>
        <br/>

        <h3>Studenci</h3>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Imię</th>
                    <th scope="col">Nazwisko</th>
                    <th scope="col">Email</th>
                    <th scope="col">Operacje</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($studentsList as $student)
                    <tr>
                        <td>{{$student->first_name}}</td>
                        <td>{{$student->last_name}}</td>
                        <td>{{$student->email}}</td>
                        <td>
                            <div class="btn-group">
                                <a type="button" class="btn btn-success m-1 btn-sm" href="{{ URL::to('assistant/users/editStudent/' . $student->id) }}">Edytuj</a>
                                <a type="button" class="btn btn-danger m-1 btn-sm" href="{{ URL::to('assistant/users/deleteStudent/' . $student->id) }}">Usuń</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <h3>Opiekunowie</h3>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Imię</th>
                    <th scope="col">Nazwisko</th>
                    <th scope="col">Email</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($assistantsList as $assistant)
                    <tr>
                        <td>{{$assistant->first_name}}</td>
                        <td>{{$assistant->last_name}}</td>
                        <td>{{$assistant->email}}</td>
                        <td></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        
    </div>
</div>



@endsection('content')
    
