@extends('layouts.template')

@section('content')

<div class="main">
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Nazwa</th>
                    <th scope="col">Model</th>
                    <th scope="col">Numer</th>
                    <th scope="col">Status</th>
                    <th scope="col">Student</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($equipmentsList as $equipment)
                    <tr>
                        <td>{{$equipment->name}}</td>
                        <td>{{$equipment->model}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @foreach ($equipment->pieces as $piece)
                        @if ($piece->checkouts_quantity == 0)
                            <tr>
                                <td></td>
                                <td></td>
                                <td>{{$piece->number}}</td>
                                <td>Dostępny</td>
                                <td></td>
                            </tr>
                        @else
                            <tr>
                                <td></td>
                                <td></td>
                                <td>{{$piece->number}}</td>
                                <td>Wypożyczony</td>
                                <td>{{$piece->checkout->student->first_name}} {{$piece->checkout->student->last_name}} - {{$piece->checkout->student->email}}</td>
                            </tr>
                        @endif

                    @endforeach
                @endforeach
            </tbody>
        </table>
    </div>
</div>



@endsection('content')