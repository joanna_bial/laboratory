@extends('layouts.template')

@section('content')

<div class="sidebar">
    <form action="{{ action('App\Http\Controllers\AssistantEquipmentController@filter')}}" method="POST" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/> 
        @foreach ($allTags as $tag)
        <div class="row">
            <input class="form-check-input me-1 form-control" type="checkbox" name="selectedTags[]" value="{{$tag->id}}" aria-label="..." <?php if(in_array($tag->id, $selectedTagsIds)) echo "checked='checked'"; ?> />
            {{$tag->name}}
        </div>
        @endforeach
        <input type="submit" value="Filtruj" class="btn btn-primary"/>
    </form>   

</div>

<div class="main">
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Fotografia</th>
                    <th scope="col">Nazwa</th>
                    <th scope="col">Model</th>
                    <th scope="col">Opis</th>
                    <th scope="col">Producent</th>
                    <th scope="col">Ilość</th>
                    <th scope="col">Dostępne</th>
                    <th scope="col">Tagi</th>
                    <th scope="col">Operacje</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($equipmentsList as $equipment)
                    <tr>
                        <td><img src="{{asset($equipment->picture_path) }}" height="100px" width="100px" alt="" title=""/></td>
                        <td>{{$equipment->name}}</td>
                        <td>{{$equipment->model}}</td>
                        <td>{{$equipment->description}}</td>
                        <td><a href="{{$equipment->producent_url}}">{{$equipment->producent_url}}</td>
                        <td>{{$equipment->quantity}}</td>
                        <td>{{$equipment->available_quantity}}</td>
                        <td>
                            @foreach ($equipment->tags as $tag)
                                <form method="post" action="{{ action('App\Http\Controllers\AssistantEquipmentController@filter')}}" class="inline">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <input type="hidden" name="selectedTags[]" value="{{$tag->id}}"/>
                                    <input type="submit" class="btn btn-outline-secondary btn-sm m-1" value="{{$tag->name}}"/>
                                </form>
                            @endforeach
                        </td>
                        <td>
                            <div class="btn-group-vertical">
                                <a type="button" class="btn btn-success m-1 btn-sm" href="{{ URL::to('assistant/equipments/edit/' . $equipment->id) }}">Edytuj</a>
                                <a type="button" class="btn btn-danger m-1 btn-sm" href="{{ URL::to('assistant/equipments/delete/' . $equipment->id) }}">Usuń</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a class="btn btn-outline-secondary" href="{{ URL::to('assistant\equipments\create') }}">Dodaj nowy element wyposażenia</a>
    </div>
</div>



@endsection('content')
    
