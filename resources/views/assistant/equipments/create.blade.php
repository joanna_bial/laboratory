@extends('layouts.template')
@section('content')


<div class="container">
    <h2>Tworzenie nowego elementu wyposażenia:</h2>

    <form action="{{ action('App\Http\Controllers\AssistantEquipmentController@store') }}" method="POST" role="form" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <div class="form-group">
            <label for="name">Nazwa</label>
            <input type="text" class="form-control" name="equipmentName"/>
        </div>

        <div class="form-group">
            <label for="model">Model</label>
            <input type="text" class="form-control" name="model"/>
        </div>

        <div class="form-group">
            <label for="description">Opis</label>
            <input type="text" class="form-control" name="description"/>
        </div>

        <div class="form-group">
            <label for="producent_url">Producent</label>
            <input type="text" class="form-control" name="producent_url"/>
        </div>

        <div class="form-group">
            <label for="image">Fotografia</label>
            <input type="file" class="form-control" name="image"/>
        </div>
        
        <div class="form-group">
            <label for="tags[]">Tagi:</label>
            @foreach ($allTags as $tag)
            <div class="row m-1">
                <input class="form-check-input form-control" type="checkbox" name="tags[]" value="{{$tag->id}}" aria-label="..."/>
                &nbsp; {{$tag->name}}
            </div>
            @endforeach
        </div>

        <div class="form-group">
            <label for="quantity">Ilość</label>
            <input type="text" class="form-control" name="quantity"/>
        </div>
        </br>

        <input type="submit" value="Zapisz" class="btn btn-primary"/>

    </form>
</div>


@endsection('content')
    