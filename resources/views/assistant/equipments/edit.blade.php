@extends('layouts.template')

@section('content')


<div class="container">
    <h2>Edycja {{$equipment->name}}</h2>

    <form action="{{ action('App\Http\Controllers\AssistantEquipmentController@editStore') }}" method="POST" role="form" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="id" value="{{ $equipment->id }}"/>

        <div class="form-group">
            <label for="name">Nazwa</label>
            <input type="text" class="form-control" name="equipmentName" value="{{$equipment->name}}"/>
        </div>

        <div class="form-group">
            <label for="name">Model</label>
            <input type="text" class="form-control" name="model" value="{{$equipment->model}}"/>
        </div>

        <div class="form-group">
            <label for="name">Opis</label>
            <input type="text" class="form-control" name="description" value="{{$equipment->description}}"/>
        </div>

        <div class="form-group">
            <label for="name">Producent</label>
            <input type="text" class="form-control" name="producent_url" value="{{$equipment->producent_url}}"/>
        </div>

        <div class="form-group">
            <label for="image">Fotografia</label>
            <input type="file" class="form-control" name="image" value="{{$equipment->picture_path}}"/>
        </div>
        <br>

        <div class="form-group">
            <label for="name">Tagi:</label>
            @foreach ($allTags as $tag)
            <div class="row m-1">
                <input class="form-check-input form-control" type="checkbox" name="tags[]" value="{{$tag->id}}" <?php if(in_array($tag->id, $equipment->tagsIds)) echo "checked='checked'"; ?> aria-label="..."/>
                &nbsp; {{$tag->name}}
            </div>
            @endforeach
        </div>
        </br>

        <input type="submit" value="Zapisz" class="btn btn-primary"/>

    </form>
</div>


@endsection('content')
    