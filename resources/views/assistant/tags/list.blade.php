@extends('layouts.template')

@section('content')

<div class="main">
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Nazwa</th>
                    <th scope="col">Operacje</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tagsList as $tag)
                    <tr>
                        <td>{{$tag->name}}</td>
                        <td>
                            <div class="btn-group">
                                <a type="button" class="btn btn-success m-1 btn-sm" href="{{ URL::to('assistant/tags/edit/' . $tag->id) }}">Edytuj</a>
                                <a type="button" class="btn btn-danger m-1 btn-sm" href="{{ URL::to('assistant/tags/delete/' . $tag->id) }}">Usuń</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a class="btn btn-outline-secondary" href="{{ URL::to('assistant/tags/create') }}">Dodaj nowy tag</a>
    </div>
</div>



@endsection('content')
    
