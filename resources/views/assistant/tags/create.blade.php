@extends('layouts.template')

@section('content')


<div class="container">
    <h2>Tworzenie nowego tagu:</h2>

    <form action="{{ action('App\Http\Controllers\TagController@store') }}" method="POST" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <div class="form-group">
            <label for="name">Nazwa</label>
            <input type="text" class="form-control" name="name"/>
        </div>

        <br/>
        <input type="submit" value="Zapisz" class="btn btn-primary"/>

        

    </form>
</div>


@endsection('content')
    