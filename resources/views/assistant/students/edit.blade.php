@extends('layouts.template')

@section('content')


<div class="container">
    <h2>Edytowanie {{$student->first_name}} {{$student->last_name}}:</h2>

    <form action="{{ action('App\Http\Controllers\StudentController@editStore') }}" method="POST" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="id" value="{{ $student->id }}"/>

        <div class="form-group">
            <label for="name">Imię</label>
            <input type="text" class="form-control" name="firstname" value="{{$student->first_name}}"/>
        </div>
        <div class="form-group">
            <label for="name">Nazwisko</label>
            <input type="text" class="form-control" name="lastname" value="{{$student->last_name}}"/>
        </div>
        <div class="form-group">
            <label for="name">Email</label>
            <input type="text" class="form-control" name="email" value="{{$student->email}}"/>
        </div>

        <br/>
        <input type="submit" value="Zapisz" class="btn btn-primary"/>

    </form>
</div>


@endsection('content')
    