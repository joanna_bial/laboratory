<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('/css/styles.css')}}"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"/>

  </head>
  <body>

  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
                        @guest
                        <div class="navbar-collapse collapse w-200 order-1 order-md-0 dual-collapse2">
                            <ul class="nav navbar-nav mr-auto">
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Logowanie') }}</a>
                                </li>
                            @endif
                            </ul>
                         </div>
                        @else
                            
                            <div class="navbar-collapse collapse w-200 order-1 order-md-0 dual-collapse2">
                                <ul class="nav navbar-nav mr-auto">
                                    @if (Auth::user()->status == 'assistant')
                                        <li class="nav-item active">
                                            <a class="nav-link" href="{{ URL::to('assistant/equipments') }}">Wyposażenie</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ URL::to('assistant/tags') }}">Tagi</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ URL::to('assistant/users') }}">Użytkownicy</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ URL::to('assistant/checkouts') }}">Wypożyczenia</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ URL::to('assistant/equipments/raport') }}">Raport</a>
                                        </li>
                                    @else
                                        <li class="nav-item active">
                                            <a class="nav-link" href="{{ URL::to('student/equipments') }}">Wyposażenie</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                            <ul class="nav navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Wyloguj') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                </li>

                                @if (Route::has('password.request'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL::to('auth/passwords/reset') }}">
                                        {{ __('Zmiana hasła') }}
                                    </a>
                                </li>
                                @endif
                            </ul>
                            
                        @endguest
            </nav>

    @yield('content')

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  </body>
</html>
