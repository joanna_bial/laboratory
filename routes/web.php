<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// List of equipments for student
Route::get('student/equipments/', 'App\Http\Controllers\StudentEquipmentController@index');
Route::post('student/equipments/', 'App\Http\Controllers\StudentEquipmentController@filter');

// List of equipments for assistant
Route::get('assistant/equipments', 'App\Http\Controllers\AssistantEquipmentController@index');
Route::post('assistant/equipments', 'App\Http\Controllers\AssistantEquipmentController@filter');

// Delete equipment
Route::get('assistant/equipments/delete/{id}', 'App\Http\Controllers\AssistantEquipmentController@delete');

// Edit equipment
Route::get('assistant/equipments/edit/{id}', 'App\Http\Controllers\AssistantEquipmentController@edit');
Route::post('assistant/equipments/edit/', 'App\Http\Controllers\AssistantEquipmentController@editStore');

// Add new equipment
Route::get('assistant/equipments/create', 'App\Http\Controllers\AssistantEquipmentController@create');
Route::post('assistant/equipments/create', 'App\Http\Controllers\AssistantEquipmentController@store');

// Raport
Route::get('assistant/equipments/raport', 'App\Http\Controllers\AssistantEquipmentController@raport');

// List of tags
Route::get('assistant/tags/', 'App\Http\Controllers\TagController@index');

// Delete tag
Route::get('assistant/tags/delete/{id}', 'App\Http\Controllers\TagController@delete');

// Edit tag
Route::get('assistant/tags/edit/{id}', 'App\Http\Controllers\TagController@edit');
Route::post('assistant/tags/edit/', 'App\Http\Controllers\TagController@editStore');

// Add new tag
Route::get('assistant/tags/create', 'App\Http\Controllers\TagController@create');
Route::post('assistant/tags/create', 'App\Http\Controllers\TagController@store');


// List of all students
Route::get('assistant/students/', 'App\Http\Controllers\StudentController@index');

// Delete student
Route::get('assistant/students/delete/{id}', 'App\Http\Controllers\StudentController@delete');

// Edit student
Route::get('assistant/students/edit/{id}', 'App\Http\Controllers\StudentController@edit');
Route::post('assistant/students/edit/', 'App\Http\Controllers\StudentController@editStore');

// Add new student
Route::get('assistant/students/create', 'App\Http\Controllers\StudentController@create');
Route::post('assistant/students/create', 'App\Http\Controllers\StudentController@store');

////////////////////////////////////////////////////////////

// List of all users
Route::get('assistant/users/', 'App\Http\Controllers\UserController@index');

// Delete student
Route::get('assistant/users/deleteStudent/{id}', 'App\Http\Controllers\UserController@deleteStudent');

// Edit student
Route::get('assistant/users/editStudent/{id}', 'App\Http\Controllers\UserController@editStudent');
Route::post('assistant/users/editStudent', 'App\Http\Controllers\UserController@editStudentStore');

// Add new student
Route::get('assistant/users/createstudent', 'App\Http\Controllers\UserController@createStudent');
Route::post('assistant/users/createstudent', 'App\Http\Controllers\UserController@storeStudent');

// Add new assistant
Route::get('assistant/users/createassistant', 'App\Http\Controllers\UserController@createAssistant');
Route::post('assistant/users/createassistant', 'App\Http\Controllers\UserController@storeAssistant');

//////////////////////////

// List of all users
Route::get('assistant/checkouts/', 'App\Http\Controllers\CheckoutController@index');

Route::get('assistant/checkouts/student/{id}', 'App\Http\Controllers\CheckoutController@studentCheckouts');

Route::get('assistant/checkouts/new/{id}', 'App\Http\Controllers\CheckoutController@newCheckout');
Route::post('assistant/checkouts/new', 'App\Http\Controllers\CheckoutController@store');

Route::get('assistant/checkouts/remove/{id}', 'App\Http\Controllers\CheckoutController@remove');

Route::get('auth/passwords/reset', 'App\Http\Controllers\Auth\ResetPasswordController@index');
Route::post('auth/passwords/reset', 'App\Http\Controllers\Auth\ResetPasswordController@update');

